package com.amarula.fileshare;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import static android.support.v4.content.FileProvider.getUriForFile;

public class MainActivity extends AppCompatActivity {
  static final String AUTHORITIES_NAME = "com.amarula.fileshare.fileprovider";
  static final String FILE_NAME = "myData.txt";

  File myExternalFile;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    File dir = new File (getFilesDir(),"test");
    dir.mkdirs();
    File file = new File(dir, FILE_NAME);
    Log.d("OSQA_PROVIDER", file.toString());
    try {
      FileOutputStream f = new FileOutputStream(file);
      PrintWriter pw = new PrintWriter(f);
      pw.println("Text shared via FileProvider");
      pw.flush();
      pw.close();
      f.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      Log.i("OSQA", "File not found");
    } catch (IOException e) {
      e.printStackTrace();
    }
    share(file);
  }

  public void share(File fileSharing){
    Log.d("OSQA_PROVIDER", "Starting activity");
    Uri contentUri = FileProvider.getUriForFile(this, AUTHORITIES_NAME, fileSharing);
    Intent intent = new Intent();
    intent.setComponent(new ComponentName("com.amarula.filereader",
        "com.amarula.filereader.MainActivity"));
    intent.setDataAndType(contentUri,"text/plain");
    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    startActivity(intent);
  }
}
