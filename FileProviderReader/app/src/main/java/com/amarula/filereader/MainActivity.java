package com.amarula.filereader;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Log.d("OSQA_READER", "Activity started");
    new MyTask().execute();

  }
  private class MyTask extends AsyncTask<String, Integer, String> {

    @Override
    protected String doInBackground(String... params) {

      Uri uri = Uri.parse("content://com.amarula.fileshare.fileprovider/internal_docs/myData.txt");
      InputStream is = null;
      StringBuilder result = new StringBuilder();
      try {
        is = getApplicationContext().getContentResolver().openInputStream(uri);
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = r.readLine()) != null) {
          result.append(line);
          Log.d("OSQA_READER", line);
        }
      } catch (FileNotFoundException e) {
        Log.d("OSQA_READER", "File not found");
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        try { if (is != null) is.close(); } catch (IOException e) { }
      }
      if (!result.toString().isEmpty()) {
        Log.d("OSQA_READER", "File read successfull");
      }
      return result.toString();
    }
  }
}
